// Svsdump dumps the contents of a Sound Velocity Sensor  MessagePack archive file as
// CSV
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"

	must "bitbucket.org/uwaploe/go-must"
)

// Timestamp format
const TS_FORMAT = "2006-01-02 15:04:05.999"

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: svsdump [options] infile outfile

Reads a MessagePack format SVS records from INFILE and writes them to
OUTFILE in CSV format.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	appendMode = flag.Bool("append", false,
		"If true, append to OUTFILE rather than overwrite")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	fin, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}

	var flags int

	if *appendMode {
		flags = os.O_APPEND | os.O_CREATE | os.O_WRONLY
	} else {
		flags = os.O_CREATE | os.O_WRONLY
	}

	fout, err := os.OpenFile(args[1], flags, 0644)
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[1], err)
	}

	rdr := must.NewSvsReader(fin)
	count := int(0)
	for rdr.Scan() {
		rec := rdr.Record()
		fmt.Fprintf(fout, "%s,%.2f\n", rec.T.UTC().Format(TS_FORMAT), rec.Sv)
		count++
	}

	if err := rdr.Err(); err != nil {
		log.Fatalf("Read error on record %d: %v", count, err)
	}
	log.Printf("Read %d records", count)
}
