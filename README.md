# Dump SVS Archive Records

Svsdump reads a MuST SVS archive file and dumps the records in CSV format.

## Installation

Visit the download page of this repository to obtain a compressed-tar
archive of the compiled binary for your OS (Linux, Windows, or MacOS).

## Usage

The `-help` option displays a usage message

``` shellsession
$ svsdump -help
Usage: svsdump [options] infile outfile

Reads a MessagePack format SVS records from INFILE and writes them to
OUTFILE in CSV format.
  -append
    	If true, append to OUTFILE rather than overwrite
  -version
    	Show program version information and exit
$
```

Convert a file to CSV, timestamps are UTC.

``` shellsession
$ svsdump sv-20190808-14.mpk sv.csv
2019/08/20 12:36:52 Read 379 records
$ head -n 2 sv.csv
2019-08-08 14:53:41.023,1489.53
2019-08-08 14:53:42.016,1489.52
$
```
